#include <Keyboard.h>
int pulsePin = 2;
int onPin = 3;
int numero = 0;
int var = 1;
int unonumbers[] = {30, 31, 32, 33, 34, 35, 36, 37, 38, 39};
int keyboardnumbers[] = {49, 50, 51, 52, 53, 54, 55, 56, 57, 48};
uint8_t buf[8] = { 0 }; //Keyboard report buffer

void setup() {
  pinMode(pulsePin, INPUT_PULLUP);
  pinMode(onPin, INPUT_PULLUP);
  Keyboard.begin();
//  Serial.begin(9600);
}

void loop() {
//While the rotary dial is rotating(onPin is tied to ground through the dial)
//count how many pulses are received on pulsePin
while (digitalRead(onPin) < 1) { 
  if (digitalRead(pulsePin) == 1) {
    if (var == 1) {
    numero = numero + 1;
    var = 0;
    delay(10);
    }
  }
  else if (digitalRead(pulsePin) == 0) {
    if (var == 0) {
    var = 1;
    delay(10);
    }
  }
}
//After onPin has been released, send the right number to the computer
if (numero > 0) {
//  sendKey(numbers[numero - 1]);	//for the arduino uno
  Keyboard.write(keyboardnumbers[numero - 1]);
  numero = 0;
}
delay(100);
}

//These are only here because I used an Arduino Uno to build this and
//the Uno doesn't work with the Arduino keyboard library so I had to
//flash custom firmware to the it to allow it to work as a keyboard.
//https://github.com/aaronjeline/Arduino-Uno-Keyboard
//On newer Arduinos like the Leonardo or Pro Micro you can just use the
//keyboard library and avoid all this hassle.
//Uncomment if you're flashing on an Arduino Uno

/*
void releaseKey() {
  buf[0] = 0;
  buf[2] = 0;
  Serial.write(buf, 8);	// Release key  
}

void sendKey(int keyNum) {
  buf[2] = keyNum;
  Serial.write(buf, 8);
  releaseKey();
}
*/
